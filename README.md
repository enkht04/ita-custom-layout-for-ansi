## Custom Keyboard layout IT for ansi keyboard
Ansil ayout misisng 1 key, no <> brackets

#Remaps 
altgr+ù = <
shift+altgr+ù = >


## Install
Run setup.exe as admin
Reboot required (for real)
Select layout from language bar
now works

## Keyboard identifiers (find custom ID)
Computer\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Keyboard Layouts\a0020410

Value 00c2
Decimal value 
00000192

## Set default keyboard layout at startup
Settings -> Time & Language -> Typing ->Advanced Keyboard settings
Select the preferred layout from the dropdown menu

or use
set_default_keyboard_layout_Custom_IT.reg
